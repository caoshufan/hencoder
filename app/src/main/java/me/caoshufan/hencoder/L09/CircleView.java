package me.caoshufan.hencoder.L09;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import me.caoshufan.hencoder.Utils;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/11/5
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class CircleView extends View {
    private static final int RADIUS = (int) Utils.dp2px(80);
    private static final int PADDING = (int) Utils.dp2px(30);

    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

    public CircleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = (RADIUS + PADDING) * 2;
        int height = (RADIUS + PADDING) * 2;

//        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
//        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
//        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
//        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

//        switch (widthMode) {
//            case MeasureSpec.EXACTLY:
//                width = widthSize;
//                break;
//            case MeasureSpec.AT_MOST:
//                width = Math.min(widthSize,width);
//                break;
//            case MeasureSpec.UNSPECIFIED:
//                break;
//        }

        width = resolveSizeAndState(width, widthMeasureSpec, 0);
        height = resolveSizeAndState(height, heightMeasureSpec, 0);
        setMeasuredDimension(width,height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(Color.RED);
        canvas.drawCircle(PADDING + RADIUS,PADDING + RADIUS,RADIUS,paint);
    }
}
