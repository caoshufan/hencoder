package me.caoshufan.hencoder.L06;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Xfermode;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import me.caoshufan.hencoder.R;
import me.caoshufan.hencoder.Utils;

/**
 * create by Devin on 2018/10/25.
 */
public class AvatarView extends View {

    private static final float WIDTH = Utils.dp2px(300);
    private static final float PADDING = Utils.dp2px(50);
    private static final float EDGE_WIDTH = Utils.dp2px(10);

    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    Xfermode xfermode = new PorterDuffXfermode(PorterDuff.Mode.SRC_IN);
    Bitmap bitmap;
    RectF saveArea = new RectF();


    public AvatarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        saveArea.set(PADDING,PADDING,PADDING+WIDTH,PADDING + WIDTH);
    }
    {
        bitmap = getAvatar((int) WIDTH);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawOval(PADDING ,PADDING,PADDING + WIDTH, PADDING + WIDTH, paint);

        int saved = canvas.saveLayer(saveArea, paint);
        canvas.drawOval(PADDING + EDGE_WIDTH,PADDING + EDGE_WIDTH, PADDING + WIDTH - EDGE_WIDTH, PADDING + WIDTH - EDGE_WIDTH,paint);
        paint.setXfermode(xfermode);
        canvas.drawBitmap(bitmap,PADDING,PADDING,paint);
        paint.setXfermode(null);
        canvas.restoreToCount(saved);
    }



    Bitmap getAvatar(int width) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(getResources(), R.drawable.avatar_rengwuxian, options);
        options.inJustDecodeBounds = false;
        options.inDensity = options.outWidth;
        options.inTargetDensity = width;
        return BitmapFactory.decodeResource(getResources(), R.drawable.avatar_rengwuxian, options);
    }
}
