package me.caoshufan.hencoder;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/10/22
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public interface   RetrofitService {
    @GET("api/today")
    Call<ResponseBody> GetToday();
}
