package me.caoshufan.hencoder.L08;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;

import me.caoshufan.hencoder.R;
import me.caoshufan.hencoder.Utils;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/10/31
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class MaterialEditText extends android.support.v7.widget.AppCompatEditText {


    // 字体大小
    private static final float TEXT_SIZE = Utils.dp2px(12);
    //顶部间距
    private static final float TEXT_MARGIN = Utils.dp2px(8);
    // 字体位置
    private static final int TEXT_VERTICAL_OFFSET = (int) Utils.dp2px(22);
    private static final int TEXT_HORIZONTAL_OFFSET = (int) Utils.dp2px(5);
    private static final int TEXT_ANIMATION_OFFSET = (int) Utils.dp2px(16);

    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

    boolean floatingLabelShown;
    float floatingLabelFraction;
    ObjectAnimator animator;

    boolean useFloatingLabelShown = true;
    Rect backgroundPadding = new Rect();

    public MaterialEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context,attrs);
    }

    private void init(Context context,AttributeSet attrs){

        // xml 使用
        TypedArray typedArray = context.obtainStyledAttributes(attrs,R.styleable.MaterialEditText);
        useFloatingLabelShown = typedArray.getBoolean(R.styleable.MaterialEditText_useFloatingLabelShown,true);
        typedArray.recycle();

        paint.setTextSize(TEXT_SIZE);

        onUseFloatingLabelChanged();

        // editText设置
        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (useFloatingLabelShown) {
                    if (!floatingLabelShown && !TextUtils.isEmpty(charSequence)) {
                        floatingLabelShown = true;
                        getAnimator().start();
                    }
                    else if(floatingLabelShown && TextUtils.isEmpty(charSequence)){
                        floatingLabelShown = false;
                        getAnimator().reverse();

                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void onUseFloatingLabelChanged() {
        getBackground().getPadding(backgroundPadding);
        if (useFloatingLabelShown) {
            setPadding(getPaddingLeft(), (int) (backgroundPadding.top+ TEXT_SIZE + TEXT_MARGIN),getPaddingRight(),getPaddingBottom());
        }else {
            setPadding(getPaddingLeft(),backgroundPadding.top,getPaddingRight(),getPaddingBottom());
        }
    }

    public void setUseFloatingLabelShown(boolean useFloatingLabelShown) {
        if (this.useFloatingLabelShown != useFloatingLabelShown) {
            this.useFloatingLabelShown = useFloatingLabelShown;
            onUseFloatingLabelChanged();
        }

    }

    private ObjectAnimator getAnimator() {
        if (animator == null) {
            animator = ObjectAnimator.ofFloat(MaterialEditText.this,"floatingLabelFraction", 0 , 1);
        }
        return animator;
    }



    public float getFloatingLabelFraction() {
        return floatingLabelFraction;
    }

    public void setFloatingLabelFraction(float floatingLabelFraction) {
        this.floatingLabelFraction = floatingLabelFraction;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        paint.setAlpha((int) (0xff * floatingLabelFraction));
        float extraOffset = TEXT_ANIMATION_OFFSET * (1 - floatingLabelFraction);
        // 绘制
        canvas.drawText(getHint().toString(),TEXT_HORIZONTAL_OFFSET, TEXT_VERTICAL_OFFSET + extraOffset,paint);

    }
}
