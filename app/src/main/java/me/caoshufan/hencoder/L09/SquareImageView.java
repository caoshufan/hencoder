package me.caoshufan.hencoder.L09;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/11/5
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class SquareImageView extends android.support.v7.widget.AppCompatImageView {

    public SquareImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int measureWidth = getMeasuredWidth();
        int measureHight = getMeasuredHeight();

        int size = Math.max(measureHight,measureWidth);

        setMeasuredDimension(size,size);
    }
}
