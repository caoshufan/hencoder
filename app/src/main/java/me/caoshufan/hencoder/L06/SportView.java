package me.caoshufan.hencoder.L06;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import me.caoshufan.hencoder.Utils;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/10/25
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class SportView extends View {

    private static final float RADIUS = Utils.dp2px(150);
    private static final float RING_WIDTH = Utils.dp2px(20);
    private static final int CIRCLE_COLOR = Color.parseColor("#90A4AE");
    private static final int HIGHLIGHT_COLOR = Color.parseColor("#FF4081");

    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    Paint.FontMetrics fontMetrics = new Paint.FontMetrics();

    public SportView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    {
        paint.setTextSize(Utils.dp2px(100));
        paint.setTextAlign(Paint.Align.CENTER);
        paint.getFontMetrics(fontMetrics);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // 画圆
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(RING_WIDTH);
        paint.setColor(CIRCLE_COLOR);
        canvas.drawOval(getWidth() / 2 - RADIUS,
                getHeight() / 2 - RADIUS ,
                getWidth() / 2 + RADIUS,
                getHeight() / 2 + RADIUS,
                paint);

        // 画弧线
        paint.setColor(HIGHLIGHT_COLOR);
        paint.setStrokeCap(Paint.Cap.ROUND);
        canvas.drawArc(getWidth() / 2 - RADIUS,
                getHeight() / 2 - RADIUS,
                getWidth() / 2 + RADIUS,
                getHeight() / 2 + RADIUS,
                -90,225,false,paint);


        // 绘制文字
        paint.setStyle(Paint.Style.FILL);
        float offset = (fontMetrics.ascent + fontMetrics.descent) / 2;
        canvas.drawText("abg",getWidth() / 2, getHeight() / 2 - offset, paint);
    }
}
