package me.caoshufan.hencoder.L07;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import me.caoshufan.hencoder.R;

public class CameraView extends View {
    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    Camera camera = new Camera();

    public CameraView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    {
        camera.rotateX(45);
        camera.setLocation(0, 0, -8 * getResources().getDisplayMetrics().density); // -8 = -8 * 72
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // 绘制上半部分
        canvas.save();
        canvas.translate(100 + 600 / 2, 100 + 600 / 2);
        canvas.rotate(-20);
        canvas.clipRect(- 600, - 600, 600, 0);
        canvas.rotate(20);
        canvas.translate(- (100 + 600 / 2), - (100 + 600 / 2));
        canvas.drawBitmap(getAvatar( 600), 100, 100, paint);
        canvas.restore();

        // 绘制下半部分
        canvas.save();
        canvas.translate(100 + 600 / 2, 100 + 600 / 2);
        canvas.rotate(-20);
        camera.applyToCanvas(canvas);
        canvas.clipRect(- 600, 0, 600, 600);
        canvas.rotate(20);
        canvas.translate(- (100 + 600 / 2), - (100 + 600 / 2));
        canvas.drawBitmap(getAvatar(600), 100, 100, paint);
        canvas.restore();
    }

    Bitmap getAvatar(int width) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(getResources(), R.drawable.avatar_rengwuxian, options);
        options.inJustDecodeBounds = false;
        options.inDensity = options.outWidth;
        options.inTargetDensity = width;
        return BitmapFactory.decodeResource(getResources(), R.drawable.avatar_rengwuxian, options);
    }
}
