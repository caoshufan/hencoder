package me.caoshufan.hencoder.L09;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/11/5
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class TabLayout extends ViewGroup {

    List<Rect> childrenBounds = new ArrayList<>();

    public TabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int widthUsed = 0;
        int heightUsed = 0;
        int lineWidthUsed = 0;
        int lineMaxHeight = 0;
        int specMode = MeasureSpec.getMode(widthMeasureSpec);
        int specWidth = MeasureSpec.getSize(widthMeasureSpec);

        for (int i = 0; i < getChildCount() ; i++) {
            View child = getChildAt(i);
            measureChildWithMargins(child,widthMeasureSpec,0,heightMeasureSpec,heightUsed);

            if (specMode != MeasureSpec.UNSPECIFIED && lineWidthUsed + child.getMeasuredWidth() > specWidth) {
                lineWidthUsed = 0;
                heightUsed += lineMaxHeight;
                lineMaxHeight = 0;
                measureChildWithMargins(child,widthMeasureSpec,0,heightMeasureSpec,heightUsed);
            }

            Rect childBound;
            if (childrenBounds.size() <= i) {
                childBound = new Rect();
                childrenBounds.add(childBound);
            }else {
                childBound = childrenBounds.get(i);
            }

            childBound.set(lineWidthUsed,heightUsed,lineWidthUsed + child.getMeasuredWidth(),heightUsed + child.getMeasuredHeight());
            lineWidthUsed += child.getMeasuredWidth();
            widthUsed = Math.max(widthUsed, lineWidthUsed);
            lineMaxHeight = Math.max(lineMaxHeight,child.getMeasuredHeight());

        }

        int width = widthUsed ;
        int height = lineMaxHeight;
        setMeasuredDimension(width,height);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        for (int i = 0 ; i < getChildCount(); i++) {
            View child = getChildAt(i);
            Rect childBound = childrenBounds.get(i);
            child.layout(childBound.left,childBound.top,childBound.right,childBound.bottom);

        }
    }

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new MarginLayoutParams(getContext(), attrs);
    }
}
